package org.zzzyxwvut.monetary.tests;

import java.io.Closeable;
import java.io.IOException;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.Writer;
import java.util.Objects;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.zzzyxwvut.ate.Result.TestResult;
import org.zzzyxwvut.ate.Testable;

class TestReporter implements Closeable
{
	private static final String EOL = System.lineSeparator();

	private final XMLStreamWriter writer;

	TestReporter(Writer writer)
	{
		Objects.requireNonNull(writer, "writer");

		try {
			this.writer = XMLOutputFactory
				.newFactory()
				.createXMLStreamWriter(writer);
		} catch (final XMLStreamException e) {
			throw new UncheckedXMLStreamException(e);
		}
	}

	synchronized void beginReport()
	{
		try {
			writer.writeStartDocument("UTF-8", "1.0");
			writer.writeCharacters(EOL);
			writer.writeStartElement("results");	// <results>
			writer.writeCharacters(EOL);
			writer.flush();
		} catch (final XMLStreamException e) {
			throw new UncheckedXMLStreamException(e);
		}
	}

	synchronized void endReport()
	{
		try {
			writer.writeEndElement();		// </results>
			writer.writeCharacters(EOL);
			writer.writeEndDocument();
			writer.flush();
		} catch (final XMLStreamException e) {
			throw new UncheckedXMLStreamException(e);
		}
	}

	synchronized void writeTestResult(Class<? extends Testable> klass,
							TestResult result)
	{
		try {
			writer.writeCharacters("\t");
			writer.writeStartElement("result");	// <result>
			writer.writeAttribute("className", klass.getName());
			writer.writeAttribute("methodNamePrefix", result
				.testMethodNamePrefix()
				.orElseThrow(IllegalStateException::new));
			writer.writeCharacters(EOL);

			writer.writeCharacters("\t\t");
			writer.writeStartElement("success");
			writer.writeCharacters(Integer.toString(
						result.successTotal()));
			writer.writeEndElement();
			writer.writeCharacters(EOL);

			writer.writeCharacters("\t\t");
			writer.writeStartElement("failure");
			writer.writeCharacters(Integer.toString(
						result.failureTotal()));
			writer.writeEndElement();
			writer.writeCharacters(EOL);

			writer.writeCharacters("\t");
			writer.writeEndElement();		// </result>
			writer.writeCharacters(EOL);
			writer.flush();
		} catch (final XMLStreamException e) {
			throw new UncheckedXMLStreamException(e);
		}
	}

	@Override
	public synchronized void close() throws IOException
	{
		try {
			writer.close();
		} catch (final XMLStreamException e) {
			throw new IOException(e);
		}
	}

	static final class UncheckedXMLStreamException extends RuntimeException
	{
		private static final long serialVersionUID = 1L;

		UncheckedXMLStreamException(XMLStreamException cause)
		{
			super(Objects.requireNonNull(cause, "cause"));
		}

		@Override
		public XMLStreamException getCause()
		{
			return (XMLStreamException) super.getCause();
		}

		private void readObject(ObjectInputStream stream) throws
							IOException,
							ClassNotFoundException
		{
			stream.defaultReadObject();

			if (!(super.getCause() instanceof XMLStreamException))
				throw new InvalidObjectException(
						"Not an XMLStreamException");
		}
	}
}
