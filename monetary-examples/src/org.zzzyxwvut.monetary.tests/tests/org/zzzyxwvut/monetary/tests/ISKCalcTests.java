package org.zzzyxwvut.monetary.tests;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.zzzyxwvut.monetary.isk.ISKCalc;

import org.zzzyxwvut.ate.Testable;
import org.zzzyxwvut.ate.configuration.Instantiable;

class ISKCalcTests implements Testable
{
	private static final ISKCalcTests INSTANCE = new ISKCalcTests();

	public static void setUpAll()
	{
		/*
		 * Abort further tests unless the separators and the range of
		 * minor unit for the locale match the expected string.
		 */
		final String obtained = ISKCalc.from("1000").toLocalisedString();
		final String expected = "ISK1.000";

		if (!expected.equals(obtained))
			throw new Error(
				"Invalid grouping separator/decimal separator/minor unit: "
							.concat(obtained));
	}

	public void testPlus(ISKCalc expected, ISKCalc augend, ISKCalc addend)
	{
		final ISKCalc obtained = augend.plus(addend);
		assert expected.equals(obtained) : obtained;
	}

	public void testMinus(ISKCalc expected, ISKCalc minuend,
							ISKCalc subtrahend)
	{
		final ISKCalc obtained = minuend.minus(subtrahend);
		assert expected.equals(obtained) : obtained;
	}

	public void testTimes(ISKCalc expected, ISKCalc multiplicand,
							ISKCalc multiplier)
	{
		final ISKCalc obtained = multiplicand.times(multiplier);
		assert expected.equals(obtained) : obtained;
	}

	public void testDividedBy(ISKCalc expected, ISKCalc dividend,
							ISKCalc divisor)
	{
		final ISKCalc obtained = dividend.dividedBy(divisor);
		assert expected.equals(obtained) : obtained;
	}

	public void testToLocalisedStringAsInput(ISKCalc expected,
							ISKCalc augend,
							ISKCalc addend)
	{
		final ISKCalc obtained = augend.plus(ISKCalc.from(
					addend.toLocalisedString()));
		assert expected.equals(obtained) : obtained;
	}

	private static void assertComparison(int expected, ISKCalc left,
							ISKCalc right)
	{
		final int obtained = left.compareTo(right);
		assert expected == obtained : obtained;
	}

	public void testCompareToGreaterThan(int expected, ISKCalc left,
							ISKCalc right)
	{
		assertComparison(expected, left, right);
	}

	public void testCompareToLessThan(int expected, ISKCalc left,
							ISKCalc right)
	{
		assertComparison(expected, left, right);
	}

	public void testCompareToEquals(int expected, ISKCalc left,
							ISKCalc right)
	{
		assertComparison(expected, left, right);
	}

	public void testEquals(ISKCalc left, ISKCalc right)
	{
		assert left.equals(right);
	}

	public void testFirstTimesThenMinus(ISKCalc expected,
							ISKCalc multiplicand,
							ISKCalc multiplier,
							ISKCalc minuend)
	{
		final ISKCalc obtained = minuend.minus(
					multiplicand.times(multiplier));
		assert expected.equals(obtained) : obtained;
	}

	public void testSorting(String expected, List<ISKCalc> obtained)
	{
		obtained.sort(ISKCalc::compareTo);
		assert expected.equals(obtained.toString()) : obtained;
	}

	public static Instantiable<ISKCalcTests> instantiable()
	{
		return Instantiable.<ISKCalcTests>newBuilder(Set.of(() ->
								INSTANCE))
			.arguments(Map.ofEntries(Map.entry("testPlus",
					List.of(List.of(ISKCalc.from("19"),
							ISKCalc.from("10"),
							ISKCalc.from(" 9")),
						List.of(ISKCalc.from("-19"),
							ISKCalc.from("-10"),
							ISKCalc.from(" -9")))),
				Map.entry("testMinus",
					List.of(List.of(ISKCalc.from("1"),
							ISKCalc.from("10"),
							ISKCalc.from(" 9")),
						List.of(ISKCalc.from(" -1"),
							ISKCalc.from("-10"),
							ISKCalc.from(" -9")))),
				Map.entry("testTimes",
					List.of(List.of(ISKCalc.from("90"),
							ISKCalc.from("10"),
							ISKCalc.from(" 9")),
						List.of(ISKCalc.from("90"),
							ISKCalc.from("-10"),
							ISKCalc.from(" -9")))),
				Map.entry("testDividedBy",
					List.of(List.of(ISKCalc.from(" 1"),
							ISKCalc.from("10"),
							ISKCalc.from(" 9")),
						List.of(ISKCalc.from(" 1"),
							ISKCalc.from("-10"),
							ISKCalc.from(" -9")))),
				Map.entry("testToLocalisedStringAsInput",
					List.of(List.of(ISKCalc.from("19"),
							ISKCalc.from("10"),
							ISKCalc.from(" 9")),
						List.of(ISKCalc.from("-19"),
							ISKCalc.from("-10"),
							ISKCalc.from(" -9")))),
				Map.entry("testCompareToGreaterThan",
					List.of(List.of(1,
							ISKCalc.from("10"),
							ISKCalc.from(" 9")))),
				Map.entry("testCompareToLessThan",
					List.of(List.of(-1,
							ISKCalc.from(" 9"),
							ISKCalc.from("10")))),
				Map.entry("testCompareToEquals",
					List.of(List.of(0,
							ISKCalc.from("10"),
							ISKCalc.from("10")))),
				Map.entry("testEquals",
					List.of(List.of(ISKCalc.from("1e3"),
							ISKCalc.from("1000")))),
				Map.entry("testFirstTimesThenMinus",
					List.of(List.of(ISKCalc.from("1002"),
							ISKCalc.from(" 9"),
							ISKCalc.from(" 1"),
							ISKCalc.from("1.011")))),
				Map.entry("testSorting",
					List.of(List.of(
						"[1, 1, 9, 10, 19, 19, 90, 1002]",
						Arrays.asList(ISKCalc.from("10"),
							ISKCalc.from(" 9"),
							ISKCalc.from("19"),
							ISKCalc.from(" 1"),
							ISKCalc.from("90"),
							ISKCalc.from(" 1"),
							ISKCalc.from("19"),
							ISKCalc.from("1.002")))))))
			.build();
	}
}
