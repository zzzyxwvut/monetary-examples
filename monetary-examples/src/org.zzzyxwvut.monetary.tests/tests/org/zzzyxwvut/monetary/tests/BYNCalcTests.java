package org.zzzyxwvut.monetary.tests;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.zzzyxwvut.monetary.byn.BYNCalc;

import org.zzzyxwvut.ate.Testable;
import org.zzzyxwvut.ate.configuration.Instantiable;

class BYNCalcTests implements Testable
{
	private static final BYNCalcTests INSTANCE = new BYNCalcTests();

	public static void setUpAll()
	{
		/*
		 * Abort further tests unless the separators and the range of
		 * minor unit for the locale match the expected string.
		 */
		final String obtained = BYNCalc.from("1000").toLocalisedString();
		final String expected = "Br1 000,00";

		if (!expected.equals(obtained))
			throw new Error(
				"Invalid grouping separator/decimal separator/minor unit: "
							.concat(obtained));
	}

	public void testPlus(BYNCalc expected, BYNCalc augend, BYNCalc addend)
	{
		final BYNCalc obtained = augend.plus(addend);
		assert expected.equals(obtained) : obtained;
	}

	public void testMinus(BYNCalc expected, BYNCalc minuend,
							BYNCalc subtrahend)
	{
		final BYNCalc obtained = minuend.minus(subtrahend);
		assert expected.equals(obtained) : obtained;
	}

	public void testTimes(BYNCalc expected, BYNCalc multiplicand,
							BYNCalc multiplier)
	{
		final BYNCalc obtained = multiplicand.times(multiplier);
		assert expected.equals(obtained) : obtained;
	}

	public void testDividedBy(BYNCalc expected, BYNCalc dividend,
							BYNCalc divisor)
	{
		final BYNCalc obtained = dividend.dividedBy(divisor);
		assert expected.equals(obtained) : obtained;
	}

	public void testToLocalisedStringAsInput(BYNCalc expected,
							BYNCalc augend,
							BYNCalc addend)
	{
		final BYNCalc obtained = augend.plus(BYNCalc.from(
					addend.toLocalisedString()));
		assert expected.equals(obtained) : obtained;
	}

	private static void assertComparison(int expected, BYNCalc left,
							BYNCalc right)
	{
		final int obtained = left.compareTo(right);
		assert expected == obtained : obtained;
	}

	public void testCompareToGreaterThan(int expected, BYNCalc left,
							BYNCalc right)
	{
		assertComparison(expected, left, right);
	}

	public void testCompareToLessThan(int expected, BYNCalc left,
							BYNCalc right)
	{
		assertComparison(expected, left, right);
	}

	public void testCompareToEquals(int expected, BYNCalc left,
							BYNCalc right)
	{
		assertComparison(expected, left, right);
	}

	public void testEquals(BYNCalc left, BYNCalc right)
	{
		assert left.equals(right);
	}

	public void testFirstTimesThenMinus(BYNCalc expected,
							BYNCalc multiplicand,
							BYNCalc multiplier,
							BYNCalc minuend)
	{
		final BYNCalc obtained = minuend.minus(
					multiplicand.times(multiplier));
		assert expected.equals(obtained) : obtained;
	}

	public void testSorting(String expected, List<BYNCalc> obtained)
	{
		obtained.sort(BYNCalc::compareTo);
		assert expected.equals(obtained.toString()) : obtained;
	}

	public static Instantiable<BYNCalcTests> instantiable()
	{
		return Instantiable.<BYNCalcTests>newBuilder(Set.of(() ->
								INSTANCE))
			.arguments(Map.ofEntries(Map.entry("testPlus",
					List.of(List.of(BYNCalc.from("20,00"),
							BYNCalc.from("10,10"),
							BYNCalc.from(" 9,90")),
						List.of(BYNCalc.from("-20,00"),
							BYNCalc.from("-10,10"),
							BYNCalc.from(" -9,90")))),
				Map.entry("testMinus",
					List.of(List.of(BYNCalc.from("0,20"),
							BYNCalc.from("10,10"),
							BYNCalc.from(" 9,90")),
						List.of(BYNCalc.from("-0,20"),
							BYNCalc.from("-10,10"),
							BYNCalc.from(" -9,90")))),
				Map.entry("testTimes",
					List.of(List.of(BYNCalc.from("99,99"),
							BYNCalc.from("10,10"),
							BYNCalc.from(" 9,90")),
						List.of(BYNCalc.from("99,99"),
							BYNCalc.from("-10,10"),
							BYNCalc.from(" -9,90")))),
				Map.entry("testDividedBy",
					List.of(List.of(BYNCalc.from(" 1,02"),
							BYNCalc.from("10,10"),
							BYNCalc.from(" 9,90")),
						List.of(BYNCalc.from(" 1,02"),
							BYNCalc.from("-10,10"),
							BYNCalc.from(" -9,90")))),
				Map.entry("testToLocalisedStringAsInput",
					List.of(List.of(BYNCalc.from("20,00"),
							BYNCalc.from("10,10"),
							BYNCalc.from(" 9,90")),
						List.of(BYNCalc.from("-20,00"),
							BYNCalc.from("-10,10"),
							BYNCalc.from(" -9,90")))),
				Map.entry("testCompareToGreaterThan",
					List.of(List.of(1,
							BYNCalc.from("10,10"),
							BYNCalc.from(" 9,90")))),
				Map.entry("testCompareToLessThan",
					List.of(List.of(-1,
							BYNCalc.from(" 9,90"),
							BYNCalc.from("10,10")))),
				Map.entry("testCompareToEquals",
					List.of(List.of(0,
							BYNCalc.from("10,10"),
							BYNCalc.from("10,1")))),
				Map.entry("testEquals",
					List.of(List.of(BYNCalc.from("1e3"),
							BYNCalc.from("1000")))),
				Map.entry("testFirstTimesThenMinus",
					List.of(List.of(BYNCalc.from(" 0,10"),
							BYNCalc.from(" 9   "),
							BYNCalc.from("  ,10"),
							BYNCalc.from(" 1,00")))),
				Map.entry("testSorting",
					List.of(List.of("[0.10, 0.20, 1.02,"
					+ " 9.90, 10.10, 20.00, 20.00, 99.99]",
						Arrays.asList(BYNCalc.from("10,10"),
							BYNCalc.from(" 9,90"),
							BYNCalc.from("20,00"),
							BYNCalc.from(" 0,20"),
							BYNCalc.from("99,99"),
							BYNCalc.from(" 1,02"),
							BYNCalc.from("20,00"),
							BYNCalc.from(" 0,10")))))))
			.build();
	}
}
