package org.zzzyxwvut.monetary.tests;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.Function;

import org.zzzyxwvut.ate.Result.OtherResult;
import org.zzzyxwvut.ate.Result.Reportable;
import org.zzzyxwvut.ate.Result.SetUpAllResult;
import org.zzzyxwvut.ate.Result.SetUpBatchResult;
import org.zzzyxwvut.ate.Result.TearDownAllResult;
import org.zzzyxwvut.ate.Result.TearDownBatchResult;
import org.zzzyxwvut.ate.Result.TestResult;
import org.zzzyxwvut.ate.Result;
import org.zzzyxwvut.ate.Testable;
import org.zzzyxwvut.ate.Tester;
import org.zzzyxwvut.ate.configuration.Configurable;
import org.zzzyxwvut.ate.support.TestablePath;

class MonetaryTester
{
	private static final Path REPORT = new TestablePath(BYNCalcTests.class,
								"modules")
		.createSiblingDirectories("reports")
		.orElseThrow(() -> new IllegalStateException(
				"Failed to create a reports directory"))
		.resolve("MonetaryTesterReport.xml");

	static {
		final ClassLoader loader = MonetaryTester.class.getClassLoader();
		loader.setClassAssertionStatus(
			"org.zzzyxwvut.monetary.tests.BYNCalcTests", true);
		loader.setClassAssertionStatus(
			"org.zzzyxwvut.monetary.tests.ISKCalcTests", true);
	}

	private MonetaryTester() { /* No instantiation. */ }

	public static void main(String... args) throws IOException
	{
		final boolean success;

		try (BufferedWriter bw = Files.newBufferedWriter(REPORT,
					StandardOpenOption.TRUNCATE_EXISTING,
					StandardOpenOption.CREATE,
					StandardOpenOption.WRITE);
				TestReporter reporter = new TestReporter(bw)) {
			try {
				reporter.beginReport();
				success = Tester.newBuilder(Set.of(
							BYNCalcTests.class,
							ISKCalcTests.class))
					.configurable(Configurable.newBuilder()
						.reportable(twoWayReporter(
								reporter))
						.build())
					.executionPolicy(Testable.ExecutionPolicy
								.CONCURRENT)
					.verifyAssertion()
					.build()
					.runAndReport();
			} finally {
				reporter.endReport();
			}
		}

		System.exit((success) ? 0 : 1);
	}

	private static Reportable twoWayReporter(TestReporter testReporter)
	{
		return Function.<Function<Map<Class<? extends Result>,
					Function<TestReporter,
					BiConsumer<Class<? extends Testable>,
								Result>>>,
				Function<Reportable,
				Function<TestReporter, Reportable>>>>
								identity()
			.apply(strategy -> resultor -> reporter ->
								stream ->
								testClass ->
								result -> {
				try {
					strategy.get(result.getClass())
						.apply(reporter)
						.accept(testClass, result);
				} finally {
					resultor.apply(stream)
						.apply(testClass)
						.accept(result);
				}
			}).apply(Map.of(SetUpAllResult.class,
				reporter -> (testClass, result) -> { },
						TearDownAllResult.class,
				reporter -> (testClass, result) -> { },
						SetUpBatchResult.class,
				reporter -> (testClass, result) -> { },
						TearDownBatchResult.class,
				reporter -> (testClass, result) -> { },
						TestResult.class,
				reporter -> (testClass, result) ->
					reporter.writeTestResult(testClass,
							(TestResult) result),
						OtherResult.class,
				reporter -> (testClass, result) -> { }))
			.apply(Result.resultor()
				.apply(Result.renderer()
					.apply(Map.of(OtherResult.class,
						OtherResult.RENDITION_REVERSE_VIDEO,
						TestResult.class,
						TestResult.RENDITION_RED_YELLOW_GREEN))))
			.apply(testReporter);
	}
}
