module org.zzzyxwvut.monetary.tests
{
	requires static org.zzzyxwvut.ate;
	requires static java.xml;

	requires org.zzzyxwvut.monetary.byn;
	requires org.zzzyxwvut.monetary.isk;

	opens org.zzzyxwvut.monetary.tests to
		org.zzzyxwvut.ate;

	provides org.zzzyxwvut.ate.support.AccessibleUplooker with
		org.zzzyxwvut.monetary.tests.TestUplooker;
}
