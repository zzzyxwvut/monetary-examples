/** Defines support for the BYN monetary type. */
module org.zzzyxwvut.monetary.byn
{
	requires transitive org.zzzyxwvut.monetary;

	exports org.zzzyxwvut.monetary.byn;
}
