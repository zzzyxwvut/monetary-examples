package org.zzzyxwvut.monetary.byn;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.Locale;
import java.util.Objects;

import org.zzzyxwvut.monetary.AbstractCalculator;
import org.zzzyxwvut.monetary.LocalisedMonetaryProperties;
import org.zzzyxwvut.monetary.LocalisedMonetaryStringFilter;
import org.zzzyxwvut.monetary.MonetaryProperties;

/** An {@code AbstractCalculator} for the BYN (933) monetary type. */
public final class BYNCalc extends AbstractCalculator<BYNCalc>
{
	private static final MonetaryProperties PROPERTIES =
		new LocalisedMonetaryProperties(new Locale("be", "BY"));
	private static final LocalisedMonetaryStringFilter LMSF =
		new LocalisedMonetaryStringFilter(PROPERTIES);

	private BYNCalc(BigDecimal value, MathContext context)
	{
		super(value, context);
	}

	/**
	 * Creates a new {@code BYNCalc} object.
	 *
	 * @param value a string representation of a monetary value
	 * @param context a numerical context
	 * @return a new {@code BYNCalc} object
	 */
	public static BYNCalc from(String value, MathContext context)
	{
		Objects.requireNonNull(value, "value");
		Objects.requireNonNull(context, "context");
		return new BYNCalc(new BigDecimal(LMSF.filter(value)), context);
	}

	/**
	 * Creates a new {@code BYNCalc} object, using
	 * the {@link java.math.MathContext#DECIMAL128} numerical context.
	 *
	 * @param value a string representation of a monetary value
	 * @return a new {@code BYNCalc} object
	 */
	public static BYNCalc from(String value)
	{
		return from(value, MathContext.DECIMAL128);
	}

	@Override
	protected BYNCalc newCalc(BigDecimal value, MathContext context)
	{
		return new BYNCalc(value, context);
	}

	@Override
	protected BigDecimal monetaryValue()
	{
		return rawValue().setScale(PROPERTIES.minorUnitDigits(),
					mathContext().getRoundingMode());
	}

	/**
	 * Returns a localised string representation of a monetary value.
	 *
	 * @return a localised string representation of a monetary value
	 */
	public String toLocalisedString()
	{
		return PROPERTIES.toLocalisedString(monetaryValue());
	}

	@Override
	public String toString()
	{
		return monetaryValue().toPlainString();
	}
}
