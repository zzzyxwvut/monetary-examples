/** Defines support for the ISK monetary type. */
module org.zzzyxwvut.monetary.isk
{
	requires transitive org.zzzyxwvut.monetary;

	exports org.zzzyxwvut.monetary.isk;
}
