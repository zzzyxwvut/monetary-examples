Examples of monetary support.

Take these steps to configure this project's build:

* supply an Apache Ivy library on the class path  
(see <https://ant.apache.org/ivy/download.cgi>)  
(see <https://ant.apache.org/manual/install.html#optionalTasks>)

* adapt the path to a local (to be) Apache Ivy repository for publishing  
(use `monetary.examples.ivy.repo.ivy2.dir` in `monetary-examples/common/ivy.properties`)

* adapt the path to a JDK-17 (or newer) compiler for the `ant` `javac` task  
(use `test.javac.executable` in `monetary-examples/common/build.properties`)

This project depends on `ate` available from
[here](https://bitbucket.org/zzzyxwvut/ate.git "here").  
This project depends on `monetary` available from
[here](https://bitbucket.org/zzzyxwvut/monetary.git "here").  
Before building this project, make a local installation of `ate` and `monetary`.

Build the project and locally publish its Apache Ivy artifacts:  
`cd repos/monetary-examples/monetary-examples/`  
`ant -projecthelp`  
`ant ivy-publish ## -lib ${IVYJAR:?}`
